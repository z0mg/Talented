﻿using ZzukBot.Game.Statics;

namespace Talented.Wrappers
{
    internal class LuaCaller : ILuaCaller
    {      
        public void Execute(string luaString)
        {
            Lua.Instance.Execute(luaString);
        }

        public string[] ExecuteAndGetText(string luaString, string[] returnVariables)
        {
            return Lua.Instance.GetText(luaString, returnVariables);
        }

        public string ExecuteAndGetText(string luaString, string returnVariable)
        {
            return Lua.Instance.GetText(luaString, returnVariable);
        }
    }
}
