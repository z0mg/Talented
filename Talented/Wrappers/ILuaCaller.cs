﻿namespace Talented.Wrappers
{
    internal interface ILuaCaller
    {
        string ExecuteAndGetText(string luaString, string returnVariable);
        string[] ExecuteAndGetText(string luaString, string[] returnVariables);
        void Execute(string luaString);
    }
}
