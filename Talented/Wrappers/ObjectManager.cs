﻿namespace Talented.Wrappers
{
    internal class ObjectManager : IObjectManager
    {
        public bool WeAreInGame()
        {
            return ZzukBot.Game.Statics.ObjectManager.Instance != null && ZzukBot.Game.Statics.ObjectManager.Instance.Player != null && ZzukBot.Game.Statics.ObjectManager.Instance.Player.Level > 0 && ZzukBot.Game.Statics.ObjectManager.Instance.IsIngame;
        }
    }
}