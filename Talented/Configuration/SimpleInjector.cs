﻿using SimpleInjector;
using System;
using Talented.Forms;
using Talented.Helpers;
using Talented.Repositories;
using Talented.Workers;
using Talented.Wrappers;

namespace Talented.Configuration
{
    internal class SimpleInjector
    {
        private static readonly Lazy<Container> ContainerInstance = new Lazy<Container>(() => new Container());

        public Container Container { get { return ContainerInstance.Value; } }

        public void Initialize()
        {
            // Helpers
            this.Container.Register<ICache, Cache>(Lifestyle.Singleton);

            // Wrappers;
            this.Container.Register<ILuaCaller, LuaCaller>(Lifestyle.Singleton);
            this.Container.Register<IObjectManager, ObjectManager>(Lifestyle.Singleton);
            this.Container.Register<IOptionManager, OptionManager>(Lifestyle.Singleton);

            // Repositories
            this.Container.Register<ITalentRepository, TalentRepository>(Lifestyle.Singleton);
            this.Container.Register<ISettingsRepository, SettingsRepository>(Lifestyle.Singleton);

            // Workers
            this.Container.Register<IWorker, TalentWorker>(Lifestyle.Transient);

            // Forms
            this.Container.Register<SettingsForm>(Lifestyle.Singleton);
            
            // Check
            this.Container.Verify();
        }
    }
}