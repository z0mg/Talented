﻿using System;
using System.ComponentModel.Composition;
using System.Threading;
using System.Threading.Tasks;
using Talented.Constants;
using Talented.Forms;
using Talented.Repositories;
using Talented.Workers;
using ZzukBot.ExtensionFramework.Interfaces;

namespace Talented
{
    [Export(typeof(IPlugin))]
    public class Talented : IPlugin
    {
        private static readonly Lazy<Configuration.SimpleInjector> simpleInjectorInstance = new Lazy<Configuration.SimpleInjector>(() => new Configuration.SimpleInjector());

        private Configuration.SimpleInjector Injector  { get { return simpleInjectorInstance.Value; } }

        private SettingsForm settingsForm;

        private CancellationTokenSource cancellationToken;

        private Random random;

        public string Author
        {
            get
            {
                return "z0mg";
            }
        }

        public string Name
        {
            get
            {
                return "Talented";
            }
        }

        public int Version
        {
            get
            {
                return 1;
            }
        }

        public bool Load()
        {
            this.Injector.Initialize();
            this.cancellationToken = new CancellationTokenSource();
            this.random = new Random();

            this.LoadSettings();

            Task.Run(async () =>
            {
                while (this.cancellationToken.IsCancellationRequested == false)
                {
                    var talentWorker = this.Injector.Container.GetInstance<IWorker>();
                    talentWorker.DoWork();
                    await Task.Delay(random.Next(50000, 70000));
                }
            });

            return true; 
        }
        
        public void ShowGui()
        {         
            if (this.settingsForm != null && this.settingsForm.Visible)
            {
                return;
            }

            this.settingsForm = this.Injector.Container.GetInstance<SettingsForm>();
            this.settingsForm.ShowDialog();
        }

        public void Unload()
        {         
            if (this.settingsForm == null)
            {
                return;
            }

            this.settingsForm.Close();
            this.settingsForm.Dispose();
            this.settingsForm = null;

            this.cancellationToken.Cancel();
        }

        public void _Dispose()
        {
            this.Unload(); 
        }

        private void LoadSettings()
        {
            var settingsRepository = this.Injector.Container.GetInstance<ISettingsRepository>();
            settingsRepository.LoadSetting(SettingKey.TalentStrings);
        }
    }
}
