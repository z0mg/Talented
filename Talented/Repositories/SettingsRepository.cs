﻿using System;
using System.Collections;
using Talented.Constants;
using Talented.Wrappers;

namespace Talented.Repositories
{
    internal class SettingsRepository : ISettingsRepository
    {
        private const string SettingsKey = "TalentedSettings";

        private readonly IOptionManager optionManager;

        private readonly Hashtable settings;

        public SettingsRepository(IOptionManager optionManager)
        {
            this.optionManager = optionManager;
            this.settings = new Hashtable();
        }

        public void AddSetting(SettingKey key, string value)
        {
            if (this.settings.ContainsKey(key) == false)
            {
                this.settings.Add(key, value);
            }
            else
            {
                this.settings[key] = value;
            }

            this.SaveSettingsToFile(key);
        }

        public void AddSetting(SettingKey key, string[] values)
        {
            if (this.settings.ContainsKey(key) == false)
            {
                this.settings.Add(key, values);
            }
            else
            {
                this.settings[key] = values;
            }

            this.SaveSettingsToFile(key);
        }

        public string[] GetTalentStrings()
        {
            if (this.settings.ContainsKey(SettingKey.TalentStrings))
            {
                return this.settings[SettingKey.TalentStrings] as string[];
            }

            return null;
        }

        public void LoadSetting(SettingKey key)
        {
            switch (key)
            {
                case SettingKey.TalentStrings:
                    var talentStrings = this.optionManager.LoadFromJson<string[]>(this.GetSettingsKeyStringForKey(key));
                    if (talentStrings != null)
                    {
                        this.AddSetting(key, talentStrings);
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException("key", key, null);
            }
        }

        private void SaveSettingsToFile(SettingKey key)
        {
            switch (key)
            {
                case SettingKey.TalentStrings:
                    var talentStrings = this.GetTalentStrings();
                    if (talentStrings != null)
                    {
                        this.optionManager.SaveToJson(this.GetSettingsKeyStringForKey(key), talentStrings);
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException("key", key, null);
            }
        }

        private string GetSettingsKeyStringForKey(SettingKey key)
        {
            return string.Format("{0}-{1}", SettingsKey, key);
        }
    }
}
