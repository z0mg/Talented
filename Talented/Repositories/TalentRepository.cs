﻿using System;
using System.Collections.Generic;
using Talented.Helpers;
using Talented.Models;
using Talented.Wrappers;

namespace Talented.Repositories
{
    internal class TalentRepository : ITalentRepository
    {
        private const string KeyUnspentTalents = "unspentTalents";
        private const string KeyTalentDictionary = "talentDictionary";

        private readonly ICache cache;
        private readonly ILuaCaller luaCaller;

        public TalentRepository(ICache cache, ILuaCaller luaCaller)
        {
            this.cache = cache;
            this.luaCaller = luaCaller;       
        }

        public int GetUnspentTalents()
        {
            return this.cache.GetOrStore(KeyUnspentTalents, () =>
            {
                return new IndexModel { Index = Convert.ToInt32(this.luaCaller.ExecuteAndGetText("TM_unspentTalentPoints, TM_learnedProfessions = UnitCharacterPoints(\"player\");", "TM_unspentTalentPoints")) };
            }, 120).Index;
        }

        public IReadOnlyList<Talent> GetTalents()
        {
            return this.cache.GetOrStore(KeyTalentDictionary, () =>
            {
                var talents = new List<Talent>();
                int tabCount = Convert.ToInt32(this.luaCaller.ExecuteAndGetText("TM_numberOfTabs = GetNumTalentTabs()", "TM_numberOfTabs"));

                for (int i = 1; i <= tabCount; i++)
                {                    
                    int talentCount = Convert.ToInt32(this.luaCaller.ExecuteAndGetText(string.Format("TM_numberOfTalents = GetNumTalents({0})", i), "TM_numberOfTalents"));
                    for (int j = 1; j <= talentCount; j++)
                    {
                        var results = this.luaCaller.ExecuteAndGetText(string.Format("TM_nameTalent, TM_icon, TM_tier, TM_column, TM_currRank, TM_maxRank = GetTalentInfo({0},{1});", i, j), new string[3] { "TM_nameTalent", "TM_currRank", "TM_maxRank" });
                        
                        var talent = new Talent(
                            results[0],
                            Convert.ToInt32(results[1]),
                            Convert.ToInt32(results[2]), 
                            i, 
                            j);

                        talents.Add(talent);
                    }
                }

                return talents;
            }, 120);
        }
        
        public void ClearCachedData()
        {
            this.cache.RemoveFromCache(KeyTalentDictionary);
            this.cache.RemoveFromCache(KeyUnspentTalents);
        }
    }
}
