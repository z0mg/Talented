﻿using Talented.Constants;

namespace Talented.Repositories
{
    internal interface ISettingsRepository
    {           
        void AddSetting(SettingKey key, string value);
        void AddSetting(SettingKey key, string[] values);
        string[] GetTalentStrings();
        void LoadSetting(SettingKey key);
    }
}