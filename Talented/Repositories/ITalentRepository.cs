﻿using System.Collections.Generic;
using Talented.Models;

namespace Talented.Repositories
{
    internal interface ITalentRepository
    {
        IReadOnlyList<Talent> GetTalents();
        int GetUnspentTalents();
        void ClearCachedData();
    }
}
