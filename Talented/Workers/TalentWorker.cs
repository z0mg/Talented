﻿using System;
using Talented.Repositories;
using Talented.Wrappers;

namespace Talented.Workers
{
    internal class TalentWorker : IWorker
    {
        private readonly ILuaCaller luaCaller;
        private readonly ISettingsRepository settingsRepository;
        private readonly ITalentRepository talentRepository;
        private readonly IObjectManager objectManager;

        public TalentWorker(ILuaCaller luaCaller, IObjectManager objectManager, ISettingsRepository settingsRepository, ITalentRepository talentRepository)
        {
            this.luaCaller = luaCaller;
            this.objectManager = objectManager;
            this.settingsRepository = settingsRepository;
            this.talentRepository = talentRepository;            
        }

        public void DoWork()
        {
            if (this.objectManager.WeAreInGame() == false)
            {
                return;
            }

            var unspentTalents = this.talentRepository.GetUnspentTalents();
            if (unspentTalents == 0)
            {
                return;
            }

            var talents = this.talentRepository.GetTalents();
            var talentStrings = this.settingsRepository.GetTalentStrings();
            for (int i = 0; i < talentStrings.Length; i++)
            {
                var talentString = talentStrings[i];
                for (int j = 0; j < talentString.Length; j++)
                {
                    var c = talentString.Substring(j, 1);
                    var number = Convert.ToInt32(c);
                    if (number > talents[j].CurrentRank && number <= talents[j].MaxRank)
                    {
                        this.luaCaller.Execute(string.Format("LearnTalent({0}, {1});", talents[j].Tab, talents[j].Index));
                        this.talentRepository.ClearCachedData();
                        return;
                    }
                }
            }
        }
    }
}
