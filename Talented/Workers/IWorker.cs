﻿namespace Talented.Workers
{
    internal interface IWorker
    {
        void DoWork();
    }
}
