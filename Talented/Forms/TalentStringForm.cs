﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Talented.Forms
{
    internal partial class TalentStringForm : Form
    {
        public string TalentString { get; private set; }

        public TalentStringForm(string talentString)
        {
            this.InitializeComponent();

            this.txtBoxTalentString.Text = talentString;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.TalentString = this.txtBoxTalentString.Text;

            if (
                this.StringArrayOnlyContainsNumbers(
                    this.TalentString.Split(new[] {Environment.NewLine}, StringSplitOptions.None)
                        .Where(ts => string.IsNullOrWhiteSpace(ts) == false)
                        .ToArray()))
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show(
                    "The given talent string(s) are not valid. Please make sure to only enter characters that quality as numbers.",
                    "Invalid talent string(s) detected", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private bool StringArrayOnlyContainsNumbers(string[] stringArray)
        {
            return stringArray.SelectMany(str => str).All(char.IsNumber);
        }
    }
}
