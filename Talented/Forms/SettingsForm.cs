﻿using System;
using System.Linq;
using System.Windows.Forms;
using Talented.Constants;
using Talented.Repositories;

namespace Talented.Forms
{
    internal partial class SettingsForm : Form
    {
        private readonly ISettingsRepository settingsRepository;
        private readonly ITalentRepository talentRepository;
        
        public SettingsForm(ISettingsRepository settingsRepository, ITalentRepository talentRepository)
        {
            this.settingsRepository = settingsRepository;
            this.talentRepository = talentRepository;

            this.InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            using (TalentStringForm dialog = new TalentStringForm(this.txtBoxTalentString.Text))
            {
                if (dialog.ShowDialog(this) == DialogResult.OK)
                {
                    this.txtBoxTalentString.Text = dialog.TalentString;
                    this.settingsRepository.AddSetting(SettingKey.TalentStrings, dialog.TalentString.Split(new[] { Environment.NewLine }, StringSplitOptions.None).Where(ts => string.IsNullOrWhiteSpace(ts) == false).ToArray());
                    this.talentRepository.ClearCachedData();
                }
            }
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            var talentStrings = this.settingsRepository.GetTalentStrings();

            if (talentStrings != null)
            {
                foreach (var str in talentStrings)
                {
                    this.txtBoxTalentString.Text += str + Environment.NewLine;
                }
            }
        }
    }
}