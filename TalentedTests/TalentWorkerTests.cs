﻿using System.Collections.Generic;
using FakeItEasy;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Talented.Models;
using Talented.Repositories;
using Talented.Workers;
using Talented.Wrappers;

namespace TalentedTests
{
    [TestClass]
    public class TalentWorkerTests
    {
        private ILuaCaller luaCaller;
        private IObjectManager objectManager;
        private ISettingsRepository settingsRepository;
        private ITalentRepository talentRepository;

        [TestInitialize]
        public void Initialize()
        {
            this.luaCaller = A.Fake<ILuaCaller>();
            this.objectManager = A.Fake<IObjectManager>();
            this.settingsRepository = A.Fake<ISettingsRepository>();
            this.talentRepository = A.Fake<ITalentRepository>();
        }

        [TestMethod]
        public void CanCreate()
        {
            var talentWorker = new TalentWorker(this.luaCaller, this.objectManager, this.settingsRepository, this.talentRepository);
            talentWorker.Should().NotBeNull();
        }



        [TestMethod]
        public void ShouldNotCallGetUnspentTalentsIfWeAreNotInGame()
        {
            A.CallTo(() => this.objectManager.WeAreInGame()).Returns(false);

            var talentWorker = new TalentWorker(this.luaCaller, this.objectManager, this.settingsRepository, this.talentRepository);

            talentWorker.DoWork();

            A.CallTo(() => this.objectManager.WeAreInGame()).MustHaveHappened();
            A.CallTo(() => this.talentRepository.GetUnspentTalents()).MustNotHaveHappened();
        }

        [TestMethod]
        public void ShouldNotCallGetTalentsIfUnspentIsZero()
        {
            A.CallTo(() => this.objectManager.WeAreInGame()).Returns(true);
            A.CallTo(() => this.talentRepository.GetUnspentTalents()).Returns(0);

            var talentWorker = new TalentWorker(this.luaCaller, this.objectManager, this.settingsRepository, this.talentRepository);

            talentWorker.DoWork();

            A.CallTo(() => this.talentRepository.GetUnspentTalents()).MustHaveHappened();
            A.CallTo(() => this.talentRepository.GetTalents()).MustNotHaveHappened();
            A.CallTo(() => this.settingsRepository.GetTalentStrings()).MustNotHaveHappened();
        }

        [TestMethod]
        public void ShouldCallGetTalentsIfUnspentIsZero()
        {
            A.CallTo(() => this.objectManager.WeAreInGame()).Returns(true);
            A.CallTo(() => this.talentRepository.GetUnspentTalents()).Returns(1);

            var talentWorker = new TalentWorker(this.luaCaller, this.objectManager, this.settingsRepository, this.talentRepository);

            talentWorker.DoWork();

            A.CallTo(() => this.talentRepository.GetUnspentTalents()).MustHaveHappened();
            A.CallTo(() => this.talentRepository.GetTalents()).MustHaveHappened();
            A.CallTo(() => this.settingsRepository.GetTalentStrings()).MustHaveHappened();
        }

        [TestMethod]
        public void ShouldFindNoWorkBecauseWeAlreadyMatchCurrentTalentString()
        {
            A.CallTo(() => this.objectManager.WeAreInGame()).Returns(true);
            A.CallTo(() => this.talentRepository.GetUnspentTalents()).Returns(1);
            A.CallTo(() => this.talentRepository.GetTalents())
                .Returns(new List<Talent>
                {
                    new Talent("someTalent", 1, 1, 1, 1),
                    new Talent("someOtherTalent", 5, 5, 1, 2),
                    new Talent("lastTalent", 2, 2, 1, 3)
                });

            A.CallTo(() => this.settingsRepository.GetTalentStrings()).Returns(new[] {"102", "152"});

            var talentWorker = new TalentWorker(this.luaCaller, this.objectManager, this.settingsRepository, this.talentRepository);

            talentWorker.DoWork();

            A.CallTo(() => this.talentRepository.GetUnspentTalents()).MustHaveHappened();
            A.CallTo(() => this.talentRepository.GetTalents()).MustHaveHappened();
            A.CallTo(() => this.settingsRepository.GetTalentStrings()).MustHaveHappened();

            A.CallTo(() => this.luaCaller.Execute(A<string>._)).MustNotHaveHappened();
            A.CallTo(() => this.talentRepository.ClearCachedData()).MustNotHaveHappened();
        }

        [TestMethod]
        public void ShouldFindWorkBecauseWeHaveToSpendOnePoint()
        {
            A.CallTo(() => this.objectManager.WeAreInGame()).Returns(true);
            A.CallTo(() => this.talentRepository.GetUnspentTalents()).Returns(1);
            A.CallTo(() => this.talentRepository.GetTalents())
                .Returns(new List<Talent>
                {
                    new Talent("someTalent", 0, 1, 1, 1),
                });

            A.CallTo(() => this.settingsRepository.GetTalentStrings()).Returns(new[] { "1" });

            var talentWorker = new TalentWorker(this.luaCaller, this.objectManager, this.settingsRepository, this.talentRepository);

            talentWorker.DoWork();

            A.CallTo(() => this.talentRepository.GetUnspentTalents()).MustHaveHappened();
            A.CallTo(() => this.talentRepository.GetTalents()).MustHaveHappened();
            A.CallTo(() => this.settingsRepository.GetTalentStrings()).MustHaveHappened();

            A.CallTo(() => this.luaCaller.Execute("LearnTalent(1, 1);")).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => this.talentRepository.ClearCachedData()).MustHaveHappened();
        }

        [TestMethod]
        public void ShouldFindWorkBecauseWeHaveToSpendAPointInOldRow()
        {
            A.CallTo(() => this.objectManager.WeAreInGame()).Returns(true);
            A.CallTo(() => this.talentRepository.GetUnspentTalents()).Returns(1);
            A.CallTo(() => this.talentRepository.GetTalents())
                .Returns(new List<Talent>
                {
                    new Talent("someTalent", 1, 1, 1, 1),
                    new Talent("someOtherTalent", 4, 5, 1, 2),
                    new Talent("lastTalent", 2, 2, 1, 3)
                });

            A.CallTo(() => this.settingsRepository.GetTalentStrings()).Returns(new[] { "102", "152" });

            var talentWorker = new TalentWorker(this.luaCaller, this.objectManager, this.settingsRepository, this.talentRepository);

            talentWorker.DoWork();

            A.CallTo(() => this.talentRepository.GetUnspentTalents()).MustHaveHappened();
            A.CallTo(() => this.talentRepository.GetTalents()).MustHaveHappened();
            A.CallTo(() => this.settingsRepository.GetTalentStrings()).MustHaveHappened();

            A.CallTo(() => this.luaCaller.Execute("LearnTalent(1, 2);")).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => this.talentRepository.ClearCachedData()).MustHaveHappened();
        }

        [TestMethod]
        public void ShouldFindWorkBecauseWeHaveToSpendAPointInOldRowOtherTab()
        {
            A.CallTo(() => this.objectManager.WeAreInGame()).Returns(true);
            A.CallTo(() => this.talentRepository.GetUnspentTalents()).Returns(1);
            A.CallTo(() => this.talentRepository.GetTalents())
                .Returns(new List<Talent>
                {
                    new Talent("someTalent", 1, 1, 1, 1),
                    new Talent("someOtherTalent", 4, 5, 2, 1),
                    new Talent("lastTalent", 2, 2, 2, 2)
                });

            A.CallTo(() => this.settingsRepository.GetTalentStrings()).Returns(new[] { "102", "152" });

            var talentWorker = new TalentWorker(this.luaCaller, this.objectManager, this.settingsRepository, this.talentRepository);

            talentWorker.DoWork();

            A.CallTo(() => this.talentRepository.GetUnspentTalents()).MustHaveHappened();
            A.CallTo(() => this.talentRepository.GetTalents()).MustHaveHappened();
            A.CallTo(() => this.settingsRepository.GetTalentStrings()).MustHaveHappened();

            A.CallTo(() => this.luaCaller.Execute("LearnTalent(2, 1);")).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => this.talentRepository.ClearCachedData()).MustHaveHappened();
        }

        [TestMethod]
        public void ShouldFindWorkTwiceBecauseWeHaveToSpendTwoPoints()
        {
            A.CallTo(() => this.objectManager.WeAreInGame()).Returns(true);
            A.CallTo(() => this.talentRepository.GetUnspentTalents()).Returns(1);
            A.CallTo(() => this.talentRepository.GetTalents())
                .Returns(new List<Talent>
                {
                    new Talent("someTalent", 1, 1, 1, 1),
                    new Talent("someOtherTalent", 3, 5, 2, 1),
                    new Talent("lastTalent", 2, 2, 2, 2)
                });

            A.CallTo(() => this.settingsRepository.GetTalentStrings()).Returns(new[] { "102", "152" });

            var talentWorker = new TalentWorker(this.luaCaller, this.objectManager, this.settingsRepository, this.talentRepository);

            talentWorker.DoWork();
            talentWorker.DoWork();

            A.CallTo(() => this.talentRepository.GetUnspentTalents()).MustHaveHappened();
            A.CallTo(() => this.talentRepository.GetTalents()).MustHaveHappened();
            A.CallTo(() => this.settingsRepository.GetTalentStrings()).MustHaveHappened();

            A.CallTo(() => this.luaCaller.Execute("LearnTalent(2, 1);")).MustHaveHappened(Repeated.Exactly.Twice);
            
            A.CallTo(() => this.talentRepository.ClearCachedData()).MustHaveHappened();
        }
    }
}
