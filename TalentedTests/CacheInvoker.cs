﻿using System;
using Talented.Helpers;

namespace TalentedTests
{
    public class CacheInvoker : ICache
    {
        public T GetOrStore<T>(string key, Func<T> action, int maxDuration = -1) where T : class
        {
            return action();
        }

        public void RemoveFromCache(string key)
        {            
        }
    }
}
