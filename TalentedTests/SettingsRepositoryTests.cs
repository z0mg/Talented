﻿using FakeItEasy;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Talented.Constants;
using Talented.Repositories;
using Talented.Wrappers;

namespace TalentedTests
{
    [TestClass]
    public class SettingsRepositoryTests
    {
        private IOptionManager optionManager;

        [TestInitialize]
        public void Initialize()
        {
            this.optionManager = A.Fake<IOptionManager>();
        }

        [TestMethod]
        public void GetTalentStringsReturnsNullWhenNotSet()
        {
            var settingsRepository = new SettingsRepository(this.optionManager);

            settingsRepository.GetTalentStrings().Should().BeNull();
        }

        [TestMethod]
        public void GetTalentStringsReturnsTheLastSet()
        {
            var settingsRepository = new SettingsRepository(this.optionManager);

            settingsRepository.AddSetting(Talented.Constants.SettingKey.TalentStrings, new[] { "MyTalentString" });
            settingsRepository.GetTalentStrings().Should().BeEquivalentTo("MyTalentString");
        }

        [TestMethod]
        public void GetTalentStringsReturnsTheLastSetMultipleLines()
        {
            var settingsRepository = new SettingsRepository(this.optionManager);

            settingsRepository.AddSetting(Talented.Constants.SettingKey.TalentStrings, new[] { "MyTalentString", "OtherTalentString" });
            settingsRepository.GetTalentStrings().Should().BeEquivalentTo("MyTalentString", "OtherTalentString");
        }

        [TestMethod]
        public void GetTalentStringsReturnsNullWhenSingleStringCalled()
        {
            var settingsRepository = new SettingsRepository(this.optionManager);

            settingsRepository.AddSetting(Talented.Constants.SettingKey.TalentStrings, "MyTalentString");
            settingsRepository.GetTalentStrings().Should().BeNull();
        }

        [TestMethod]
        public void CanLoadSettingsFromJsonFile()
        {
            A.CallTo(() => this.optionManager.LoadFromJson<string[]>(A<string>.That.IsEqualTo("TalentedSettings-TalentStrings"))).Returns(new[]
            {
                "05000505000000000000000000000000000000000000000000",
                "55000505000010000000000000000000000000000000000000",
                "55000535002010050005030000000000000000000000000000",
                "55000535002010050005230005050000000000000000000000"
            });

            var settingsRepository = new SettingsRepository(this.optionManager);
            settingsRepository.LoadSetting(SettingKey.TalentStrings);
            settingsRepository.GetTalentStrings().Length.Should().Be(4);
            settingsRepository.GetTalentStrings()[0].ShouldBeEquivalentTo("05000505000000000000000000000000000000000000000000");
            settingsRepository.GetTalentStrings()[1].ShouldBeEquivalentTo("55000505000010000000000000000000000000000000000000");
            settingsRepository.GetTalentStrings()[2].ShouldBeEquivalentTo("55000535002010050005030000000000000000000000000000");
            settingsRepository.GetTalentStrings()[3].ShouldBeEquivalentTo("55000535002010050005230005050000000000000000000000");
        }

    }
}
