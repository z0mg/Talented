﻿using System;
using FakeItEasy;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Talented.Helpers;
using Talented.Repositories;
using Talented.Wrappers;

namespace TalentedTests
{
    [TestClass]
    public class TalentRepositoryTests
    {
        private ICache cache;
        private ILuaCaller luaCaller;

        [TestInitialize]
        public void Initialize()
        {
            this.cache = new CacheInvoker();
            this.luaCaller = A.Fake<ILuaCaller>();
        }

        [TestMethod]
        public void CanCreate()
        {
            new TalentRepository(this.cache, this.luaCaller).Should().NotBeNull();
        }

        [TestMethod]
        public void ShouldClearCache()
        {
            var fakeCache = A.Fake<ICache>();

            var talentRepository = new TalentRepository(fakeCache, this.luaCaller);
            talentRepository.ClearCachedData();

            A.CallTo(() => fakeCache.RemoveFromCache("unspentTalents")).MustHaveHappened();
            A.CallTo(() => fakeCache.RemoveFromCache("talentDictionary")).MustHaveHappened();
        }

        [TestMethod]
        public void GetUnspentTalentsShouldBe0()
        {
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_unspentTalentPoints, TM_learnedProfessions = UnitCharacterPoints(\"player\");", "TM_unspentTalentPoints")).Returns("0");

            var talentRepository = new TalentRepository(this.cache, this.luaCaller);

            talentRepository.GetUnspentTalents().Should().Be(0);
        }


        [TestMethod]
        public void GetUnspentTalentsShouldBe2()
        {
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_unspentTalentPoints, TM_learnedProfessions = UnitCharacterPoints(\"player\");", "TM_unspentTalentPoints")).Returns("2");

            var talentRepository = new TalentRepository(this.cache, this.luaCaller);

            talentRepository.GetUnspentTalents().Should().Be(2);
        }

        [TestMethod]
        public void GetUnspentTalentsShouldBe100()
        {
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_unspentTalentPoints, TM_learnedProfessions = UnitCharacterPoints(\"player\");", "TM_unspentTalentPoints")).Returns("100");

            var talentRepository = new TalentRepository(this.cache, this.luaCaller);

            talentRepository.GetUnspentTalents().Should().Be(100);
        }

        [TestMethod]
        public void GetUnspentTalentsShouldBe0WhenNoReturnValueFromLua()
        {
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_unspentTalentPoints, TM_learnedProfessions = UnitCharacterPoints(\"player\");", "TM_unspentTalentPoints")).Returns(null);

            var talentRepository = new TalentRepository(this.cache, this.luaCaller);

            talentRepository.GetUnspentTalents().Should().Be(0);            
        }

        [ExpectedException(typeof(FormatException))]
        [TestMethod]
        public void GetUnspentTalentsThrowExceptionWhenEmptyReturnValueFromLua()
        {
            var talentRepository = new TalentRepository(this.cache, this.luaCaller);

            talentRepository.GetUnspentTalents();
        }

        [ExpectedException(typeof(FormatException))]
        [TestMethod]
        public void GetUnspentTalentsShouldThrowExceptionWhenNoNumberReturnedFromLua()
        {
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_unspentTalentPoints, TM_learnedProfessions = UnitCharacterPoints(\"player\");", "TM_unspentTalentPoints")).Returns("ABC");

            var talentRepository = new TalentRepository(this.cache, this.luaCaller);

            talentRepository.GetUnspentTalents().Should().Be(100);
        }

        [ExpectedException(typeof(FormatException))]
        [TestMethod]
        public void GetTalentsShouldThrowExceptionWhenEmptyReturnValueFromLua()
        {
            var talentRepository = new TalentRepository(this.cache, this.luaCaller);

            talentRepository.GetTalents();
        }

        [ExpectedException(typeof(FormatException))]
        [TestMethod]
        public void GetTalentsShouldThrowExceptionWhenNoNumberReturnedFromLua()
        {
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_numberOfTabs = GetNumTalentTabs()", "TM_numberOfTabs")).Returns("ABC");

            var talentRepository = new TalentRepository(this.cache, this.luaCaller);

            talentRepository.GetTalents();
        }

        [TestMethod]
        public void GetTalentsShouldBeEmptyWhenTabCountIs0()
        {
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_numberOfTabs = GetNumTalentTabs()", "TM_numberOfTabs")).Returns("0");

            var talentRepository = new TalentRepository(this.cache, this.luaCaller);

            talentRepository.GetTalents().Should().BeEmpty();
        }

        [TestMethod]
        public void GetTalentsShouldBeEmptyWhenNoNumberReturnedFromLua()
        {
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_numberOfTabs = GetNumTalentTabs()", "TM_numberOfTabs")).Returns(null);

            var talentRepository = new TalentRepository(this.cache, this.luaCaller);

            talentRepository.GetTalents().Should().BeEmpty();
        }

        [ExpectedException(typeof(FormatException))]
        [TestMethod]
        public void GetTalentsShouldBeEmptyWhenTabCountIs1ButTalentCountOnTabReturnValueIsNotANumber()
        {
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_numberOfTabs = GetNumTalentTabs()", "TM_numberOfTabs")).Returns("1");
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_numberOfTalents = GetNumTalents(1)", "TM_numberOfTalents")).Returns("ABC");

            var talentRepository = new TalentRepository(this.cache, this.luaCaller);

            talentRepository.GetTalents().Should();
        }

        [ExpectedException(typeof(FormatException))]
        [TestMethod]
        public void GetTalentsShouldBeEmptyWhenTabCountIs1ButTalentCountOnTabReturnValueIsEmpty()
        {
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_numberOfTabs = GetNumTalentTabs()", "TM_numberOfTabs")).Returns("1");            

            var talentRepository = new TalentRepository(this.cache, this.luaCaller);

            talentRepository.GetTalents().Should();
        }

        [TestMethod]
        public void GetTalentsShouldBeEmptyWhenTabCountIs1ButTalentCountOnTabIs0()
        {
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_numberOfTabs = GetNumTalentTabs()", "TM_numberOfTabs")).Returns("1");
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_numberOfTalents = GetNumTalents(1)", "TM_numberOfTalents")).Returns("0");

            var talentRepository = new TalentRepository(this.cache, this.luaCaller);

            talentRepository.GetTalents().Should().BeEmpty();
        }

        [TestMethod]
        public void GetTalentsShouldContainOneTalentWhenTabCountIs1AndTalentCountOnTabIs1()
        {
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_numberOfTabs = GetNumTalentTabs()", "TM_numberOfTabs")).Returns("1");
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_numberOfTalents = GetNumTalents(1)", "TM_numberOfTalents")).Returns("1");
            A.CallTo(() => this.luaCaller.ExecuteAndGetText(string.Format("TM_nameTalent, TM_icon, TM_tier, TM_column, TM_currRank, TM_maxRank = GetTalentInfo({0},{1});", 1, 1), A<string[]>.That.IsSameSequenceAs(new string[3] { "TM_nameTalent", "TM_currRank", "TM_maxRank" }))).Returns(new[] { "someTalentName", "1", "1" });

            var talentRepository = new TalentRepository(this.cache, this.luaCaller);

            talentRepository.GetTalents().Count.Should().Be(1);
            talentRepository.GetTalents()[0].Name.Should().Be("someTalentName");
            talentRepository.GetTalents()[0].CurrentRank.Should().Be(1);
            talentRepository.GetTalents()[0].MaxRank.Should().Be(1);
            talentRepository.GetTalents()[0].Tab.Should().Be(1);
            talentRepository.GetTalents()[0].Index.Should().Be(1);
        }

        [TestMethod]
        public void GetTalentsShouldContainOneTalentWhenTabCountIs1AndTalentCountOnTabIs1WithNoRank()
        {
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_numberOfTabs = GetNumTalentTabs()", "TM_numberOfTabs")).Returns("1");
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_numberOfTalents = GetNumTalents(1)", "TM_numberOfTalents")).Returns("1");
            A.CallTo(() => this.luaCaller.ExecuteAndGetText(string.Format("TM_nameTalent, TM_icon, TM_tier, TM_column, TM_currRank, TM_maxRank = GetTalentInfo({0},{1});", 1, 1), A<string[]>.That.IsSameSequenceAs(new string[3] { "TM_nameTalent", "TM_currRank", "TM_maxRank" }))).Returns(new[] { "someTalentName", "0", "1" });

            var talentRepository = new TalentRepository(this.cache, this.luaCaller);

            talentRepository.GetTalents().Count.Should().Be(1);
            talentRepository.GetTalents()[0].Name.Should().Be("someTalentName");
            talentRepository.GetTalents()[0].CurrentRank.Should().Be(0);
            talentRepository.GetTalents()[0].MaxRank.Should().Be(1);
            talentRepository.GetTalents()[0].Tab.Should().Be(1);
            talentRepository.GetTalents()[0].Index.Should().Be(1);
        }

        [TestMethod]
        public void GetTalentsShouldContainTwoTalentsWhenTabCountIs1AndTalentCountOnTabIs2()
        {
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_numberOfTabs = GetNumTalentTabs()", "TM_numberOfTabs")).Returns("1");
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_numberOfTalents = GetNumTalents(1)", "TM_numberOfTalents")).Returns("2");
            A.CallTo(() => this.luaCaller.ExecuteAndGetText(string.Format("TM_nameTalent, TM_icon, TM_tier, TM_column, TM_currRank, TM_maxRank = GetTalentInfo({0},{1});", 1, 1), A<string[]>.That.IsSameSequenceAs(new string[3] { "TM_nameTalent", "TM_currRank", "TM_maxRank" }))).Returns(new[] { "someTalentName", "1", "1" });
            A.CallTo(() => this.luaCaller.ExecuteAndGetText(string.Format("TM_nameTalent, TM_icon, TM_tier, TM_column, TM_currRank, TM_maxRank = GetTalentInfo({0},{1});", 1, 2), A<string[]>.That.IsSameSequenceAs(new string[3] { "TM_nameTalent", "TM_currRank", "TM_maxRank" }))).Returns(new[] { "someOtherTalentName", "0", "1" });

            var talentRepository = new TalentRepository(this.cache, this.luaCaller);

            talentRepository.GetTalents().Count.Should().Be(2);
            talentRepository.GetTalents()[0].Name.Should().Be("someTalentName");
            talentRepository.GetTalents()[0].CurrentRank.Should().Be(1);
            talentRepository.GetTalents()[0].MaxRank.Should().Be(1);
            talentRepository.GetTalents()[0].Tab.Should().Be(1);
            talentRepository.GetTalents()[0].Index.Should().Be(1);
            talentRepository.GetTalents()[1].Name.Should().Be("someOtherTalentName");
            talentRepository.GetTalents()[1].CurrentRank.Should().Be(0);
            talentRepository.GetTalents()[1].MaxRank.Should().Be(1);
            talentRepository.GetTalents()[1].Tab.Should().Be(1);
            talentRepository.GetTalents()[1].Index.Should().Be(2);
        }


        [TestMethod]
        public void GetTalentsShouldContainThreeTalentsWhenTabCountIs2AndTalentCountOnTabIs2And1()
        {
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_numberOfTabs = GetNumTalentTabs()", "TM_numberOfTabs")).Returns("2");
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_numberOfTalents = GetNumTalents(1)", "TM_numberOfTalents")).Returns("2");
            A.CallTo(() => this.luaCaller.ExecuteAndGetText("TM_numberOfTalents = GetNumTalents(2)", "TM_numberOfTalents")).Returns("1");
            A.CallTo(() => this.luaCaller.ExecuteAndGetText(string.Format("TM_nameTalent, TM_icon, TM_tier, TM_column, TM_currRank, TM_maxRank = GetTalentInfo({0},{1});", 1, 1), A<string[]>.That.IsSameSequenceAs(new string[3] { "TM_nameTalent", "TM_currRank", "TM_maxRank" }))).Returns(new[] { "someTalentName", "1", "1" });
            A.CallTo(() => this.luaCaller.ExecuteAndGetText(string.Format("TM_nameTalent, TM_icon, TM_tier, TM_column, TM_currRank, TM_maxRank = GetTalentInfo({0},{1});", 1, 2), A<string[]>.That.IsSameSequenceAs(new string[3] { "TM_nameTalent", "TM_currRank", "TM_maxRank" }))).Returns(new[] { "someOtherTalentName", "0", "1" });
            A.CallTo(() => this.luaCaller.ExecuteAndGetText(string.Format("TM_nameTalent, TM_icon, TM_tier, TM_column, TM_currRank, TM_maxRank = GetTalentInfo({0},{1});", 2, 1), A<string[]>.That.IsSameSequenceAs(new string[3] { "TM_nameTalent", "TM_currRank", "TM_maxRank" }))).Returns(new[] { "lastTalentName", "1", "5" });            

            var talentRepository = new TalentRepository(this.cache, this.luaCaller);

            talentRepository.GetTalents().Count.Should().Be(3);
            talentRepository.GetTalents()[0].Name.Should().Be("someTalentName");
            talentRepository.GetTalents()[0].CurrentRank.Should().Be(1);
            talentRepository.GetTalents()[0].MaxRank.Should().Be(1);
            talentRepository.GetTalents()[0].Tab.Should().Be(1);
            talentRepository.GetTalents()[0].Index.Should().Be(1);
            talentRepository.GetTalents()[1].Name.Should().Be("someOtherTalentName");
            talentRepository.GetTalents()[1].CurrentRank.Should().Be(0);
            talentRepository.GetTalents()[1].MaxRank.Should().Be(1);
            talentRepository.GetTalents()[1].Tab.Should().Be(1);
            talentRepository.GetTalents()[1].Index.Should().Be(2);
            talentRepository.GetTalents()[2].Name.Should().Be("lastTalentName");
            talentRepository.GetTalents()[2].CurrentRank.Should().Be(1);
            talentRepository.GetTalents()[2].MaxRank.Should().Be(5);
            talentRepository.GetTalents()[2].Tab.Should().Be(2);
            talentRepository.GetTalents()[2].Index.Should().Be(1);
        }
    }
}

